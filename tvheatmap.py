import argparse
import sys
import json
import seaborn as sns
import matplotlib.pyplot as plt
from imdb import IMDb


def make_heatmap(ia, series_id):
    try:
        with open(series_id + '.json') as f:
            file_data = json.loads(f.read())
            title = file_data['title']
            data = file_data['data']
    except Exception:
        print('Getting tvshow informations...')

        series = ia.get_movie(series_id)
        ia.update(series, 'episodes')
        title = series['title']

        maxlen = max(len(series['episodes'][s]) for s in series['episodes'])

        data = [[] for s in series['episodes']]

        for s in sorted(series['episodes']):
            for e in sorted(series['episodes'][s]):
                if 'rating' in series['episodes'][s][e].keys():
                    data[s - 1].append(series['episodes'][s][e]['rating'])
                else:
                    data[s - 1].append(0)
            for i in range(len(series['episodes'][s]), maxlen):
                data[s - 1].append(0)

        with open(series_id + '.json', 'w') as f:
            file_data = {'title': title, 'data': data}
            f.write(json.dumps(file_data))

    mask = [[(c == 0) for c in r] for r in data]
    data = list(map(list, zip(*data)))

    sns.set()
    ax = sns.heatmap(data,
                     cmap='RdYlGn',
                     annot=True,
                     cbar=False,
                     mask=mask,
                     xticklabels=range(1,
                                       len(data[0]) + 1),
                     yticklabels=range(1,
                                       len(data) + 1),
                     vmin=min(
                         min(c if c != 0 else 10 for r in data for c in r), 5),
                     vmax=10)
    ax.set_title(title)
    ax.tick_params(axis='x', length=0, labeltop=True, labelbottom=False)
    ax.xaxis.set_label_position('top')
    plt.xlabel('Season')
    plt.ylabel('Episode')
    plt.show()


def search(ia, title):
    results = [r for r in ia.search_movie(title) if r['kind'] == 'tv series']
    selected = -1
    if len(results) == 1:
        selected = 0
    while selected not in range(10):
        print('Results:')
        for i, r in enumerate(results[:10]):
            print(str(i + 1) + '.' + r['title'])
        selected = int(input('Select: ')) - 1
    return results[selected]


ia = IMDb()
parser = argparse.ArgumentParser(description='TV Heatmap')

parser.add_argument('--id',
                    nargs='?',
                    type=int,
                    help='IMDb ID of a TV show to get')
parser.add_argument('--search',
                    nargs='*',
                    help='TV show title to search on IMDb')

args = parser.parse_args()

if args.id is not None:
    tvshow_id = args.id
elif args.search is not None:
    tvshow_id = search(ia, ' '.join(args.search)).movieID
else:
    parser.print_help()
    sys.exit(1)

make_heatmap(ia, str(tvshow_id))
