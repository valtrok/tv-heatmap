# TV Heatmap

I love all the TV shows heatmaps released on [r/dataisbeautiful](https://www.reddit.com/r/dataisbeautiful/) so I decided to write this simple script so I can generate them for any TV show I want.

# Setup

Just install dependencies:

- seaborn
- imdbpy

# Usage

With an IMDb ID:

`python tvheatmap.py -id 0096697`



With a query:

`python tvheatmap.py -search Brooklyn nine nine`
